<?php

use App\Http\Controllers\Admin\CompetitionController;
use App\Http\Controllers\Admin\TeamController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Front\MatchController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'middleware' => 'api',
    'prefix' => 'admin'

], function ($router) {
    Route::post('/login', [UserController::class, 'login']);
    Route::post('/register', [UserController::class, 'register']);
    Route::post('/logout', [UserController::class, 'logout']);
    Route::post('/refresh', [UserController::class, 'refresh']);
    Route::get('/user-profile', [UserController::class, 'userProfile']);
    Route::resource('/teams', TeamController::class);
    Route::resource('/competitions',CompetitionController::class);
    Route::resource('/matches',App\Http\Controllers\Admin\MatchController::class);
});

Route::group([
    'middleware' => 'api',
    'prefix' => 'front'

], function ($router) {
    Route::get('/competitions/matches', [MatchController::class,'getComWithMatch']);
    Route::resource('/matches', MatchController::class);
});
