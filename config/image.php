<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Image Driver
    |--------------------------------------------------------------------------
    |
    | Intervention Image supports "GD Library" and "Imagick" to process images
    | internally. You may choose one of them according to your PHP
    | configuration. By default PHP's "GD Library" implementation is used.
    |
    | Supported: "gd", "imagick"
    |
    */

    'driver' => 'gd',
    'thumbnail_width'=>250,
    'thumbnail_height'=>250,
    'medium_width'=>500,
    'medium_height'=>500,
    'logo' => env('LOGO','/default-photo/logo.png'),
    'avatar' => env('AVATAR','/default-photo/avatar.png'),
    'image' => env('IMAGE','/default-photo/image.png'),

];
