<?php

namespace App\Providers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('base64image', function ($attribute, $value, $parameters, $validator) {
            $arrays= is_array($value) ? $value:array($value);
            foreach ($arrays as $array){
                $explode = explode(',', $array);
                $allow = ['jpeg', 'png', 'jpg', 'gif', 'svg'];
                $format = str_replace(['data:image/', ';', 'base64',], ['', '', '',], $explode[0]);

                // check file format
                if (!in_array($format, $allow)) {
                    return false;
                }

                // check base64 format
                if (!preg_match('%^[a-zA-Z0-9/+]*={0,2}$%', $explode[1])) {
                    return false;
                }
            }

            return true;
        },
            'The :attribute must be base64 image type.'
        );
    }
}
