<?php

namespace App\Http\Controllers\Front;

use App\Helper\Helper;
use App\Http\Controllers\Controller;
use App\Models\Competition;
use App\Models\File;
use App\Models\MatchFixture;
use App\Models\Team;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class MatchController extends Controller
{

    public function index(Request $request): JsonResponse
    {
        //Get Category data
        $items = MatchFixture::orderBy('created_at', 'DESC');

        $items = $items->paginate($this->pagination);
        $data['current_page'] = $items->currentPage();
        $data['last_page'] = $items->lastPage();
        $data['items'] = $items->items();

        return Helper::apiResponseSuccess('Get item list success',$data);
    }

    public function show($id,Request $request): JsonResponse
    {
        try {
            $item = MatchFixture::findOrFail($id);
        }catch (\Exception $e){
            return Helper::apiResponseError(Helper::DATA_NOT_EXIST_CODE,'This item does not exist');
        }
        return Helper::apiResponseSuccess('Show item success',$item->toArray());
    }

    public function getComWithMatch(Request $request): JsonResponse
    {
        //Get Category data
        $items = Competition::with('matches')->orderBy('created_at', 'DESC');

        $items = $items->paginate($this->pagination);
        $data['current_page'] = $items->currentPage();
        $data['last_page'] = $items->lastPage();
        $data['items'] = $items->items();

        return Helper::apiResponseSuccess('Get item list success',$data);
    }
}
