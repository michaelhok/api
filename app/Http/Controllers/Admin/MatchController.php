<?php

namespace App\Http\Controllers\Admin;

use App\Helper\Helper;
use App\Http\Controllers\Controller;
use App\Models\MatchFixture;
use App\Models\Team;
use http\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class MatchController extends Controller
{
    public function index(Request $request){
        $items = MatchFixture::orderBy('created_at','desc');
        $items = $items->paginate($this->pagination);

        $data['current_page'] = $items->currentPage();
        $data['last_page'] = $items->lastPage();
        $data['items'] = $items->items();

        return Helper::apiResponseSuccess('Get item list successfully',$data);
    }

    public function show($id){
        try {
            $item= MatchFixture::findOrFail($id);
        }catch (\Exception $e){
            return Helper::apiResponseError(Helper::DATA_NOT_EXIST_CODE, $e->getMessage());
        }

        return Helper::apiResponseSuccess('Get item list successfully',$item->toArray());
    }

    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'name'=> 'required|string|min:3',
            'description'=> 'nullable|string',
            'competition_id'=> 'required|numeric|exists:competitions,id',
            'home_team_id'=>'required|numeric|exists:teams,id',
            'away_team_id'=>'required|numeric|exists:teams,id',
            'user_id'=>'required|numeric|exists:users,id',
            'match_date_time'=>'required|date_format:Y-m-d H:i:s',
            'embed_video'=>'nullable|string',
            'status'=> 'nullable|numeric|min:1|max:2',
        ]);

        if($validator->fails()){
            return Helper::apiResponseError(Helper::MISSING_PARAMETER_CODE,implode(", ",$validator->errors()->all()));
        }

        MatchFixture::create(array_merge($validator->validated(), [
                'number' => Helper::genCode('match_fixtures','number')
            ]
        ));

        return Helper::apiResponseSuccess('Item created successfully.');
    }

    public function update(Request $request, $id){
        $validator = Validator::make($request->all(), [
            'name'=> 'required|string|min:3',
            'description'=> 'nullable|string',
            'competition_id'=> 'required|numeric|exists:competitions,id',
            'home_team_id'=>'required|numeric|exists:teams,id',
            'away_team_id'=>'required|numeric|exists:teams,id',
            'user_id'=>'required|numeric|exists:users,id',
            'match_date_time'=>'required|date_format:Y-m-d H:i:s',
            'embed_video'=>'nullable|string',
            'status'=> 'nullable|numeric|min:1|max:2',
        ]);

        if($validator->fails()){
            return Helper::apiResponseError(Helper::MISSING_PARAMETER_CODE,implode(", ",$validator->errors()->all()));
        }

        try {
            $item= MatchFixture::findOrFail($id);
            $item->update($validator->validated());
        } catch (\Exception $e) {
            return Helper::apiResponseError(Helper::DATA_NOT_EXIST_CODE, 'This item does not exist');
        }

        return Helper::apiResponseSuccess('This item update successfully');
    }

    public function destroy($id){
        try {
            $item= MatchFixture::findOrFail($id);
            $item->delete();

        }catch (\Exception $e){
            return Helper::apiResponseError(Helper::DATA_NOT_EXIST_CODE,'This item not exist');
        }

        return Helper::apiResponseSuccess('This items was deleted successfully');
    }
}
