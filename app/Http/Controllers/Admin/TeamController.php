<?php

namespace App\Http\Controllers\Admin;

use App\Helper\Helper;
use App\Http\Controllers\Controller;
use App\Models\Team;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class TeamController extends Controller
{
    public function index(Request $request){
        $items = Team::orderBy('created_at','desc');
        $items = $items->paginate($this->pagination);

        $data['current_page'] = $items->currentPage();
        $data['last_page'] = $items->lastPage();
        $data['items'] = $items->items();

        return Helper::apiResponseSuccess('Get item list successfully',$data);
    }

    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'name'=> 'required|string|min:3',
            'short_name'=> 'nullable|string|max:3',
            'description'=> 'nullable|string',
            'photo'=>'required|base64image',
            'type'=> 'nullable|numeric|min:1|max:2',
            'status'=> 'nullable|numeric|min:1|max:2',
        ]);

        if($validator->fails()){
            return Helper::apiResponseError(Helper::MISSING_PARAMETER_CODE,implode(", ",$validator->errors()->all()));
        }

        $photo=Helper::uploadImage([$request->photo],'teams');
        Team::create(array_merge($validator->validated(), [
                'number' => Helper::genCode('teams','number'),
                'file_id'=> json_decode( $photo->content() )->data[0],
            ]
        ));

        return Helper::apiResponseSuccess('Item created successfully.');
    }

    public function update(Request $request, $id){
        $validator = Validator::make($request->all(), [
            'name'=> 'required|string|min:3',
            'short_name'=> 'nullable|string|max:3',
            'description'=> 'nullable|string',
            'photo'=>'nullable|base64image',
            'type'=> 'nullable|numeric|min:1|max:2',
            'status'=> 'nullable|numeric|min:1|max:2',
        ]);

        if($validator->fails()){
            return Helper::apiResponseError(Helper::MISSING_PARAMETER_CODE,implode(", ",$validator->errors()->all()));
        }

        try {
            $item= Team::findOrFail($id);
            if($request->photo){
                Helper::removeFiles([$item->file_id], $item->getTable(),true);
                $photo=Helper::uploadImage([$request->photo],$item->getTable());
                $file_id= array('file_id'=> json_decode( $photo->content() )->data[0]);
                $input=array_merge($validator->validated(), $file_id);
            }
            $item->update($input ?? $validator->validated());
        } catch (\Exception $e) {
            return Helper::apiResponseError(Helper::DATA_NOT_EXIST_CODE, 'This item does not exist');
        }

        return Helper::apiResponseSuccess('This item update successfully');
    }

    public function destroy($id){
        try {
            $item= Team::findOrFail($id);
            $item->delete();

        }catch (\Exception $e){
            return Helper::apiResponseError(Helper::DATA_NOT_EXIST_CODE,'This item not exist');
        }

        return Helper::apiResponseSuccess('This items was deleted successfully');
    }
}
