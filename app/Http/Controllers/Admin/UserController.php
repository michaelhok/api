<?php

namespace App\Http\Controllers\Admin;

use App\Helper\Helper;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class UserController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('admin:api', ['except' => ['login', 'register']]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function login(Request $request): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'username' => 'required|string',
            'password' => 'required|string|min:6',
        ]);

        if ($validator->fails()) {
            return Helper::apiResponseError(Helper::MISSING_PARAMETER_CODE,implode(", ",$validator->errors()->all()));
        }

        if (! $token = auth()->attempt($validator->validated())) {
            return Helper::apiResponseError(Helper::UNAUTHORIZED_CODE,'Unauthorized');
        }

        return $this->createNewToken($token);
    }

    /**
     * Register a User.
     *
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function register(Request $request): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'username' => 'required|string|between:2,100|unique:users',
            'name' => 'required|string|between:2,100',
            'email' => 'required|string|email|max:100|unique:users',
            'password' => 'required|string|confirmed|min:6',
            'photo'=>'required|base64image',
        ]);

        if($validator->fails()){
            return Helper::apiResponseError(Helper::MISSING_PARAMETER_CODE,implode(", ",$validator->errors()->all()));
        }

        $photo=Helper::uploadImage([$request->photo],'users');
        $user = User::create(array_merge(
            $validator->validated(), [
                'password' => bcrypt($request->password),
                'number' => Helper::genCode('users','number'),
                'file_id'=> json_decode( $photo->content() )->data[0]
            ]
        ));

        return Helper::apiResponseSuccess('User successfully registered',$user->toArray());
    }


    /**
     * Log the user out (Invalidate the token).
     *
     * @return JsonResponse
     */
    public function logout() {
        auth()->logout();

        return Helper::apiResponseSuccess('User successfully signed out');
    }

    /**
     * Refresh a token.
     *
     * @return JsonResponse
     */
    public function refresh(): JsonResponse
    {
        return $this->createNewToken(auth()->refresh());
    }

    /**
     * Get the authenticated User.
     *
     * @return JsonResponse
     */
    public function userProfile(): JsonResponse
    {
        return Helper::apiResponseSuccess('User successfully registered',auth()->user()->toArray());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return JsonResponse
     */
    protected function createNewToken($token): JsonResponse
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60,
            'user' => auth()->user()
        ]);
    }
}
