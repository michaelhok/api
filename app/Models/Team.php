<?php

namespace App\Models;

use App\Helper\Traits\DateSerializable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Team extends Model
{
    use HasFactory, SoftDeletes, DateSerializable;
    protected $fillable = ['number','name','short_name', 'description','file_id','type','status'];

    public function file() {
        return $this->belongsTo(File::class);
    }

    public function toArray(){
        $array = parent::toArray();
        $array['logo']= @$this->file->path;
        return $array;
    }
}
