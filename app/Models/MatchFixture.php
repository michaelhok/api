<?php

namespace App\Models;

use App\Helper\Traits\DateSerializable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MatchFixture extends Model
{
    use HasFactory, SoftDeletes, DateSerializable;
    protected $fillable = ['number','name', 'description','competition_id','home_team_id','away_team_id','user_id','match_date_time','embed_video','status'];

    public function home_team() {
        return $this->belongsTo(Team::class, 'home_team_id');
    }

    public function away_team() {
        return $this->belongsTo(Team::class,'away_team_id');
    }

    public function competition() {
        return $this->belongsTo(Competition::class);
    }

    public function toArray(){
        $array = parent::toArray();
        $date = Carbon::parse(@$this->match_date_time);
        $array['match_date']= $date->format('M d Y');
        $array['match_time']= $date->format('h:i A');
        $array['home_logo']= @$this->home_team->file->path;
        $array['home_name']= @$this->home_team->name;
        $array['away_logo']= @$this->away_team->file->path;
        $array['away_name']= @$this->away_team->name;
        $array['competition_name']= @$this->competition->name;
        $array['competition_logo']= @$this->competition->file->path;
        return $array;
    }
}
