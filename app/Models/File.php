<?php

namespace App\Models;

use App\Helper\Traits\DateSerializable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    use HasFactory, DateSerializable;
    protected $fillable = ['name','path', 'thumbnail','format','size'];
}
