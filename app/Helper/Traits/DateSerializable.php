<?php

namespace App\Helper\Traits;


use DateTimeInterface;

trait DateSerializable
{
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

}
