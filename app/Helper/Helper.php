<?php
namespace App\Helper;

use App\Models\File;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class Helper
{

    //constant for api response header
    const HEADER = [
        'Content-Type' => 'application/json',
        'Accept' => 'multipart/form-data',
        'Authorization' => 'multipart/form-data',
    ];

    const UNAUTHORIZED_CODE = 401;
    const MISSING_PARAMETER_CODE = 422;
    const DATA_NOT_EXIST_CODE = 2;
    const OPERATION_ERROR_CODE = 3;

    //define for soft delete and restore function
    const TRASH_CONCAT= '--';
    const TRASH_FOLDER='/trash/';

    /**
     * @desc return api response success
     * @param string|null $msg
     * @param array|null $data
     * @param array $additional_field
     * @return JsonResponse
     * @date 10/09/2020
     * @author Phen
     */
    public static function apiResponseSuccess(string $msg = null, array $data = null, array $additional_field = []): JsonResponse{
        $response = [];
        if (isset($data)) {
            if (isset($msg)) {
                $response = [
                    'status' => 1,
                    'code' => 200,
                    'info' => $msg,
                    'data' => $data,

                ];
            } else {
                $response = [
                    'status' => 1,
                    'code' => 200,
                    'data' => $data,
                ];
            }
        } else if (isset($msg))
            $response = [
                'status' => 1,
                'code' => 200,
                'info' => $msg
            ];
        else
            $response = [
                'status' => 1,
                'code' => 200
            ];
        if (sizeof($additional_field) > 0)
            $response = array_merge($additional_field, $response);

        return response()->json($response, 200, self::HEADER);
    }

    /**
     * @desc return api response fail
     * @param int $code
     * @param null $msg
     * @param null $data
     * @return JsonResponse
     * @date 10/09/2020
     * @author Phen
     */
    public static function apiResponseError(int $code = 1, $msg = null, $data = null): JsonResponse{
        $code = (int)$code;
        if (isset($data)) {
            $response = [
                'status' => 0,
                'code' => $code,
                'info' => $msg,
                'data' => $data,

            ];
        } else {
            $response = [
                'status' => 0,
                'code' => $code,
                'info' => $msg
            ];
        }
        return response()->json($response, 200, self::HEADER);
    }

    public static function genCode($table, $field, $length=10, $collection=null, $table2=null){
        $min = str_repeat(0, $length-1). 1;
        $max = str_repeat(9, $length);
        $genCode = [
            $field => mt_rand($min,$max)
        ];

        if($collection) {
            if($collection->contains($field, $genCode[$field]))
                return self::genCode($collection, $field, true);
        }

        $rules = [$field => 'unique:'.$table];
        $rules = array_merge($rules, $table2 ? [$field => 'unique:'.$table2]:[]);
        $validate = Validator::make($genCode, $rules)->passes();
        return $validate ? str_pad($genCode[$field], $length, '0', STR_PAD_LEFT) : self::genCode($table,$field,$length);

    }

    /**
     * @desc return random unique code
     * @param array $base64_images
     * @param string $folder
     * @return mixed
     * @date 21/09/2020
     * @author Hok
     */
    public static function uploadImage($base64_images= array(),$folder = null){
        $image_ids=array();
        $folder_path= config('filesystems.filesystem_folder').'/'.($folder ?? config('filesystems.default_folder'));
        foreach ($base64_images as $image) {
            try {
                list($base_type, $image) = explode(';base64,', $image);
                list(,$extension) = explode('/',$base_type);
                $image = base64_decode($image);
                //Generate image name with unique ID
                $image_name = uniqid();
                $path = $folder_path.'/'.$image_name.'.'.$extension;
                $thumbnail_path= $folder_path.'/'.$image_name.'_thumbnail.'.$extension;
                $thumbnail = Image::make($image)->resize(config('image.thumbnail_width'), config('image.thumbnail_height'),
                    function ($constraint) {
                        $constraint->aspectRatio();
                    })->encode($extension);
                Storage::put($path,$image); //upload original image
                Storage::put($thumbnail_path,$thumbnail->encoded); //upload thumbnail image
                $file = File::create(array(
                    "name" => basename($path),
                    "path" => Storage::url($path),
                    "thumbnail" => Storage::url($thumbnail_path),
                    "format" => $extension,
                    "size" => Storage::size($path)
                ));
            }
            catch (\Exception $e){
                return self::apiResponseError(self::OPERATION_ERROR_CODE, $e->getMessage());
            }

            $image_ids[] = $file->id;

        }
        return self::apiResponseSuccess('upload image success', $image_ids);
    }

    /**
     * @desc delete file records in files table by array of id ex:[1,2]
     * @param array $file_ids
     * @param string $folder
     * @param bool $permanent_delete
     * @return JsonResponse
     * @date 25 Sep 2020
     * @author Phen
     */
    public static function removeFiles($file_ids = array(),$folder = null, $permanent_delete = false)
    {
        try {
            $folder_path= config('filesystems.filesystem_folder').'/'.($folder ?? config('filesystems.default_folder'));
            File::whereIn('id',$file_ids)->each(function ($file) use ($folder, $permanent_delete, $folder_path) {
                $data=$file->title;
                $trash_data=config('filesystems.filesystem_folder').self::TRASH_FOLDER.$data;
                if(!Storage::exists($data)){
                    if(Storage::exists($folder_path.'/'.$data)){
                        $data=$folder_path.'/'.$file->title;
                        $trash_data = config('filesystems.filesystem_folder').self::TRASH_FOLDER.$folder.self::TRASH_CONCAT.$file->title;
                    }
                }
                if($permanent_delete){
                    $file->forceDelete();
                    Storage::delete($data);
                }else{
                    //will move file to trash folder
                    $file->delete();
                    Storage::move($data, $trash_data);
                }
            });
        }
        catch (\Exception $e){
            return self::apiResponseError(self::OPERATION_ERROR_CODE, $e->getMessage());
        }

        return self::apiResponseSuccess('Files Deleted Successfully');
    }
}
