<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teams', function (Blueprint $table) {
            $table->id();
            $table->uuid('number')->unique();
            $table->string('name');
            $table->char('short_name',3)->nullable();
            $table->text('description')->nullable();
            $table->bigInteger('file_id')->default(0);
            $table->enum('type',[1,2])->default(1)->comment('1=Club, 2=National Team');
            $table->enum('status',[1,2])->default(1)->comment('1=Normal, 2=Disabled');
            $table->softDeletes('deleted_at', 0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teams');
    }
}
