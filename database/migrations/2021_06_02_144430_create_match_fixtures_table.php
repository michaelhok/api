<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMatchFixturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('match_fixtures', function (Blueprint $table) {
            $table->id();
            $table->uuid('number')->unique();
            $table->string('name');
            $table->text('description')->nullable();
            $table->mediumInteger('competition_id');
            $table->mediumInteger('home_team_id');
            $table->mediumInteger('away_team_id');
            $table->mediumInteger('user_id');
            $table->text('embed_video')->nullable();
            $table->enum('status',[1,2])->default(1)->comment('1=Normal, 2=Disabled');
            $table->timestamp('match_date_time')->nullable();
            $table->softDeletes('deleted_at', 0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('match_fixtures');
    }
}
